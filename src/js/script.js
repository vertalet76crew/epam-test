window.onload = (e) => {

  Weather = GetWeather();
  let Counter = 0;
  let step = 0;
  let currentDate = new Date();
  for (let item of Weather) {

    item.date = new Date(Date.now());
    item.date.setDate(item.date.getDate() + step);
    step++
  //   Counter++;
    //  if (Counter > 4) {
    //    break;
    //}
    const cssClass = getClassName(item);
    const title = getTitle(cssClass);

    let WeatherTemplate = `<div class="weather-box">
			<div class="day-caption">${currentDate.getDay() == item.date.getDay() ? "Сегодня" : item.date.toLocaleString('ru', {weekday: "long"})}</div>
			<div class="day-name bold-text">${item.date.toLocaleString('ru',{month:"long",day:"numeric"})}</div>
			<div class="weather-icon ${cssClass}"></div>
			<div class="day-temp bold-text">днем ${item.temperature.day}&deg</div>
			<div class="night-temp small-text">ночью ${item.temperature.night}&deg</div>
			<div class="is-cloudiness small-text">${title}</div>
		</div>`;
    document.querySelector('.wrapper').innerHTML += WeatherTemplate;
  }
  $('.wrapper').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<div class="arrows arrow-left">&#8249;</div>',
    nextArrow: '<div class="arrows arrow-right">&#8250;</div>',
    infinite: false,
      responsive: [
  
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
};

function GetWeather() {
  return [
    {
      date: 1538337600000,
      temperature: {
        night: -3,
        day: 2,
      },
      cloudiness: 'Ясно',
      snow: false,
      rain: false,
		  },
    {
      date: 1538424000000,
      temperature: {
        night: 0,
        day: 4,
      },
      cloudiness: 'Облачно',
      snow: false,
      rain: true,
		  },
    {
      date: 1538510400000,
      temperature: {
        night: 0,
        day: 1,
      },
      cloudiness: 'Облачно',
      snow: true,
      rain: true,
		  },
    {
      date: 1538510400000,
      temperature: {
        night: 0,
        day: 1,
      },
      cloudiness: 'Облачно',
      snow: false,
      rain: true,
		  },
    {
      date: 1538510400000,
      temperature: {
        night: 0,
        day: 1,
      },
      cloudiness: 'Облачно',
      snow: true,
      rain: true,
		  }
	];

}

// Возвращаем css-класс для элемента

function getClassName(item) {
  if (!item.rain && !item.snow) {
    return "sun";
  };

  if (!item.rain && item.snow) {
    return "snow";
  };

  if (item.rain && item.snow) {
    return "snow-rain";
  };

  if (item.rain && !item.snow) {
    return "rain";
  };
}

// Возвращаем title для типа осадков

function getTitle(type) {
  const titles = {
    sun: "без осадков",
    snow: "снег",
    rain: "дождь",
    ["snow-rain"]: "снег с дождем"
  };
  return titles[type] || "не известно";
}
console.log(getTitle)